# build stage
FROM node:12.18.3 as build-stage
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build


# production stage
FROM nginx
# Copy config nginx
COPY --from=build-stage /app/.nginx/nginx.conf /etc/nginx/conf.d/default.conf
# Remove default nginx static assets
COPY --from=build-stage /app/.next /usr/share/nginx/buffer
CMD ["nginx", "-g", "daemon off;"]